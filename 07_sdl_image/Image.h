#pragma once

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <string>

class Image {
public:
    Image(std::string Filename, SDL_Surface* WindowSurface) {
        SDL_Surface* Temp { IMG_Load(Filename.c_str()) };
        Surface = SDL_ConvertSurface(Temp, WindowSurface->format, 0);
        SDL_FreeSurface(Temp);
        SDL_BlitSurface(Surface, nullptr, WindowSurface, nullptr);
    }

    ~Image() {
        SDL_FreeSurface(Surface);
    }

private:
    SDL_Surface* Surface;
};
