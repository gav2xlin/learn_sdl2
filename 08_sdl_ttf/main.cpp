#include <SDL2/SDL.h>
#include "TextWindow.h"

int main(int argc, char** argv) {
    SDL_Init(SDL_INIT_VIDEO);

    TextWindow Window;
    Window.SetText("Hello world!");

    SDL_Event Event;
    while (true) {
        while (SDL_PollEvent(&Event)) {
            if (Event.type == SDL_QUIT) [[unlikely]] {
                SDL_Quit();
                return 0;
            }
        }
    }

    return 0;
}
