#pragma once

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <iostream>
#include <string>

using std::cout, std::endl;

class TextWindow {
public:
    TextWindow() {
        SDL_Init(SDL_INIT_VIDEO);

        SDLWindow = SDL_CreateWindow(
            "Hello Window",
            SDL_WINDOWPOS_UNDEFINED,
            SDL_WINDOWPOS_UNDEFINED,
            600, 150, 0
            );

        SDLWindowSurface = SDL_GetWindowSurface(SDLWindow);
        FillWindowSurface();
        SDL_UpdateWindowSurface(SDLWindow);

        if (TTF_Init() < 0) {
            cout << "Error calling TTF_Init: "
                 << TTF_GetError() << endl;
        }

        LoadFont();
    }

    ~TextWindow() {
        TTF_Quit();
        TTF_CloseFont(Font);
    }

    void SetText(std::string Text) {
        SDL_Color Color { 20, 20, 20 };
        SDL_Surface* TextSurface {
            TTF_RenderUTF8_Blended(Font, Text.c_str(), Color)
        };
        if (!TextSurface) {
            cout << "Failed to render text: "
                 << TTF_GetError() << endl;
        }

        FillWindowSurface();

        SDL_Rect Source { 0, 0, TextSurface->w, TextSurface->h };
        SDL_Rect Destination { 70, 40, Source.w, Source.h };
        SDL_BlitSurface(
            TextSurface, &Source, SDLWindowSurface, &Destination
            );
        SDL_FreeSurface(TextSurface);
        SDL_UpdateWindowSurface(SDLWindow);
    }

private:
    void FillWindowSurface() {
        SDL_FillRect(
            SDLWindowSurface,
            nullptr,
            SDL_MapRGB(SDLWindowSurface->format, 240, 240, 240)
            );
    }

    void LoadFont() {
        Font = TTF_OpenFont("Roboto-Medium.ttf", 50);
        if (!Font) {
            cout << "Failed to load font: "
                 << TTF_GetError() << endl;
        }
    }

    SDL_Window* SDLWindow{nullptr};
    SDL_Surface* SDLWindowSurface{nullptr};
    TTF_Font* Font;
};
