#pragma once

#include <SDL2/SDL.h>

class Window {
public:
    Window() {
        SDL_Init(SDL_INIT_VIDEO);

        SDLWindow = SDL_CreateWindow(
            "Hello Window",
            SDL_WINDOWPOS_UNDEFINED,
            SDL_WINDOWPOS_UNDEFINED,
            800, 300, 0
            );

        SDLWindowSurface = SDL_GetWindowSurface(SDLWindow);
        Update();
    }

    void Update() {
        SDL_FillRect(
            SDLWindowSurface,
            nullptr,
            SDL_MapRGB(SDLWindowSurface->format, 40, 40, 40)
            );
    }

    void RenderFrame() {
        SDL_UpdateWindowSurface(SDLWindow);
    }

    SDL_Surface* GetSurface() {
        return SDLWindowSurface;
    }

    ~Window() {
        SDL_DestroyWindow(SDLWindow);
    }

private:
    SDL_Window* SDLWindow { nullptr };
    SDL_Surface* SDLWindowSurface { nullptr };
};
