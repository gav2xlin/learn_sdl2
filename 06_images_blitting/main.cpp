#include <SDL2/SDL.h>
#include "Window.h"
#include "Image.h"

int main() {
    Window GameWindow;
    Image Birds { "birds.bmp", GameWindow.GetSurface()};

    SDL_Event Event;
    while(true) {
        while(SDL_PollEvent(&Event)) {
            if (Event.type == SDL_QUIT) {
                SDL_Quit();
                return 0;
            }
        }
        GameWindow.RenderFrame();
    }
}
