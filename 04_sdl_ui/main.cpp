#include <SDL2/SDL.h>
#include <vector>
#include <iostream>
#include "Window.h"

class EventReceiver {
public:
    virtual bool HandleEvent(const SDL_Event* Event) {
        std::cout << "It's working!!\n";
        return false;
    }
};

class Layer {
public:
    bool HandleEvent(const SDL_Event* Event) {
        for (const auto Handler : Subscribers) {
            if (Handler->HandleEvent(Event)) {
                return true;
            }
        }
        return false;
    }

    void SubscribeToEvents(EventReceiver* Receiver) {
        Subscribers.push_back(Receiver);
    }

private:
    std::vector<EventReceiver*> Subscribers;
};

int main() {
    Window GameWindow;
    Layer UI;
    Layer World;
    EventReceiver ExampleButton;
    UI.SubscribeToEvents(&ExampleButton);

    SDL_Event Event;
    while(true) {
        while(SDL_PollEvent(&Event)) {
            if (Event.type == SDL_QUIT) {
                SDL_Quit();
                return 0;
            }
            if (UI.HandleEvent(&Event)) {
                // The UI took care of this event
                // Continue to the next
                continue;
            }
            // The UI did not handle this event
            // Let the next layer see it
            if (World.HandleEvent(&Event)) {
                continue;
            }
        }
    }
}
