#include <SDL2/SDL.h>
#include "Window.h"

int main() {
    Window GameWindow;
    SDL_Event Event;

    while(true) {
        while (SDL_PollEvent(&Event)) {
            if (Event.type == SDL_QUIT) [[unlikely]] {
                SDL_Quit();
                return 0;
            }
        }
    }
}
